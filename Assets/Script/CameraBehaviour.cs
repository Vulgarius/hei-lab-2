﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    [SerializeField]private Transform target;
    [SerializeField]private float speed = 10.0f;
    [SerializeField] private Vector3 distance;
    [SerializeField] private Transform lookTarget;
    
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 dPos = target.position + distance;
        Vector3 sPos = Vector3.Lerp(transform.position, dPos, speed * Time.deltaTime);
        transform.position = sPos;
        //transform.LookAt(lookTarget.position);
    }
}
