﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using UnityEngine.UI;

public class FileReportsScreen : MonoBehaviour
{
    [SerializeField] private string path; // Path where are all the reports
    public GameObject fileButtonPrefab;
    public Transform FileListContent;
    public Text reportScreen;
    private string[] reportNames;

    private List<string> tempNumber;
    // Start is called before the first frame update
    void Start()
    {
        //Files directory
        path = Application.persistentDataPath + "/Relatorios/";

        tempNumber = new List<string>();
        GoGetAllFiles();

        
        

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void GoGetAllFiles() // Get all the files
    {
        // Theck if Folder exists
        if (Directory.Exists(path))
        {

            reportNames = Directory.GetFiles(path, "*txt");
            if(reportNames.Length == 0) // if there's no reports
            {
                Debug.LogWarning("Have no reports");
            }
        }
        else // if doesn't exist, create it
        {
            Debug.Log("Directory not available");
            Directory.CreateDirectory(path);

            reportNames = Directory.GetFiles(path, "*txt");

            if (reportNames.Length == 0) // if there's no reports
            {
                Debug.LogWarning("Have no reports");
            }

        }
        CreateList();
    }
    private void CreateList()
    {
        List<string> reportList;
        int auxiliar = 1;

        // Criar a lista com os nomes dos arquivos
        reportList = reportNames.ToList();

        foreach(string reportname in reportList)
        {
            tempNumber.Add(reportname.Replace(reportname, auxiliar.ToString()));
            auxiliar++;
        }
        for (int i = 0; i < tempNumber.Count; i++)
        {
            GameObject buttonInstance = Instantiate(fileButtonPrefab, FileListContent);
            buttonInstance.GetComponentInChildren<Text>().text = " Report " + tempNumber[i];
            //buttonInstance.GetComponent<Filebuttonbehaviour>().Setindex(i);
        }

    }
    public void Showfile(int database)
    {
        Debug.Log(database);
        reportScreen.gameObject.SetActive(true);
        //Open the file, read it and close it
        StreamReader streamReader = new StreamReader(reportNames[database]);
        var fileContents = streamReader.ReadToEnd();
        reportScreen.text = fileContents.ToString();
        streamReader.Close();
        transform.gameObject.SetActive(false);

    }
}
