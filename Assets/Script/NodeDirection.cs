﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeDirection : MonoBehaviour
{
    private PlayerBehaviour player;
    private enum Direction
    {
        Vertical, Horizontal,
    }
    [SerializeField] private Direction direction;
    private BoxCollider bc;
    //A delegate is a type that represents references to methods with a particular parameter list and return type. 
    //When you instantiate a delegate, you can associate its instance with any method with a compatible signature and return type. 
    //You can invoke (or call) the method through the delegate instance.
    public delegate void AllowedDirection(); // is delegate
    public delegate void NotAllowedDirection(); // is a delegate
    private AllowedDirection  canDirectionaction; 
    private NotAllowedDirection cannotDirectionaction;
    private bool isPlayerOn;
    private bool isAtTheEnds = false;
    [SerializeField] private Transform MaxLimit;
    [SerializeField] private Transform MinLimit;
    [SerializeField]private float differenceBetween = 6;
    [SerializeField] private float value;
    private void Awake()
    {
        player = FindObjectOfType<PlayerBehaviour>();
        bc = GetComponent<BoxCollider>();
        
    }
    // Start is called before the first frame update
    void Start()
    {
        switch (direction)
        {
            case Direction.Horizontal:
                canDirectionaction = player.CanMoveHorizontal;
                cannotDirectionaction = player.CannotMoveHorizontal;
                break;
            case Direction.Vertical:
                canDirectionaction = player.CanMoveVertical;
                cannotDirectionaction = player.CannotMoveVertical;
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        switch (direction)
        {
            case Direction.Horizontal: // Player can move left or right only;
                if (isPlayerOn)
                {

                    if (Mathf.Abs((MaxLimit.position.z - player.transform.position.z)) <= differenceBetween)
                    {
                        if (!isAtTheEnds)
                        {
                            player.CannotMoveRight();


                            //if (player.canMoveUp && player.canMoveDown)
                            if (player.currentMoving == PlayerBehaviour.Moving.isMovingUp || player.currentMoving == PlayerBehaviour.Moving.isMovingDown)
                            {
                                return;
                            }



                            if (!player.isPlayedStopped)
                            {
                                player.StopVelocity();
                            }
                            
                            //player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, MaxLimit.position.z);

                            isAtTheEnds = true;
                            
                            
                        }


                    }
                    else if (Mathf.Abs((MinLimit.position.z - player.transform.position.z)) <= differenceBetween)
                    {
                        if (!isAtTheEnds)
                        {
                            player.CannotMoveLeft();

                            //if (player.canMoveUp && player.canMoveDown)
                            if (player.currentMoving == PlayerBehaviour.Moving.isMovingUp || player.currentMoving == PlayerBehaviour.Moving.isMovingDown)
                            {
                                return;
                            }

                            //player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, MinLimit.position.z);
                            if (!player.isPlayedStopped)
                            {
                                player.StopVelocity();
                            }
                            
                            isAtTheEnds = true;
                            
                        }

                    }
                    else
                    {
                        isAtTheEnds = false;
                    }
                    
                    
                }             
                    break;
            case Direction.Vertical: // Move Up or Down
                if (isPlayerOn)
                {

                    if(Mathf.Abs(( MinLimit.position.x - player.transform.position.x)) <= differenceBetween)
                    {
                        if (!isAtTheEnds)
                        {
                            player.CannotMoveDown();

                            //if (player.canMoveLeft && player.canMoveRight)
                            if (player.currentMoving == PlayerBehaviour.Moving.isMovingLeft || player.currentMoving == PlayerBehaviour.Moving.isMovingRight)
                            {
                                return;
                            }

                            //player.transform.position = new Vector3(MinLimit.position.x, player.transform.position.y, player.transform.position.z);
                            if (!player.isPlayedStopped)
                            {
                                player.StopVelocity();
                            }
                            
                            isAtTheEnds = true;
                            
                            
                                
                        }


                    }
                    else if (Mathf.Abs((MaxLimit.position.x - player.transform.position.x)) <= differenceBetween)
                    {
                        if (!isAtTheEnds)
                        {
                            player.CannotMoveUp();


                            if (player.currentMoving == PlayerBehaviour.Moving.isMovingLeft || player.currentMoving == PlayerBehaviour.Moving.isMovingRight)
                            {
                                return;
                            }

                            //player.transform.position = new Vector3(MaxLimit.position.x, player.transform.position.y, player.transform.position.z);
                            if (!player.isPlayedStopped)
                            {
                                player.StopVelocity();
                            }
                            
                            isAtTheEnds = true;
                            
                            
                        }


                    }
                    else
                    {
                        isAtTheEnds = false;
                    }
                    

                    
                }

                break;
        }
    }
    private void OnTriggerEnter(Collider other) 
    {
        isPlayerOn = true;
        canDirectionaction();
        //if (player.isPlayedStopped)
        //{
        //    isAtTheEnds = true;
        //}
    }
    private void OnTriggerStay(Collider other)
    {
        if (!isAtTheEnds) // put back the 2 directions
        {
            canDirectionaction();
            
        }
        
        
            
        
        
    }
    private void OnTriggerExit(Collider other)
    {
        
        isPlayerOn = false;
        isAtTheEnds = false;
        cannotDirectionaction();
        
    }
}
