﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallHorizontalMoving : MonoBehaviour
{
    private enum Direction
    {
        Left,Right
    }
    [SerializeField] private Direction direction;
    [SerializeField] private Transform limitLeft, limitRight;
    [SerializeField] private int movement = 1; // this is gonna say if it's positive or negative
    [SerializeField] private float speed = 75f;
    // Start is called before the first frame update
    void Start()
    {
        if(transform.childCount != 0) // Some ball has childs that would limit of the direction.
        {
            
            transform.DetachChildren();
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        switch (direction)
        {
            case Direction.Left:
                if (movement > 0) movement = -movement;

                Moving();
                
                if(transform.position.z <= limitLeft.position.z)
                {
                    direction = Direction.Right;
                }
                break;
            case Direction.Right:
                if (movement < 0) movement = -movement;

                Moving();
                if (transform.position.z >= limitRight.position.z)
                {
                    direction = Direction.Left;
                }
                break;
        }
    }
    private void Moving() // In both directions he's got to move
    {
        transform.position += new Vector3(0, 0, movement * speed * Time.deltaTime);
    }
}
