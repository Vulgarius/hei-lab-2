﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChanceEmissionIntensityMaterial : MonoBehaviour
{
    [SerializeField]private Material material;
    private Color color;
    private float intensity = 2;
    [SerializeField]private float intensityMax = 2;
    [SerializeField]private float intensityMin = 0.5f;
    private bool isIncreasing = false;
    [SerializeField]private float direction = -1;
    private float chancebloomspeed = 0.5f;
    private float differenceBetween = 0.3f;
    private Vector4 defaultVector;
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);
        material.DisableKeyword("_EMISSION");
        color = material.GetColor("_EmissionColor");
        intensity = intensityMax;
        defaultVector = material.GetVector("_EmissionColor");      
    }

    // Update is called once per frame
    void Update()
    {
        if(material != null)
        {
            ChanceBloomIntensity();
        }
    }
    private void ChanceBloomIntensity() // Chance the Bloom intensity of the material
    {
        if (isIncreasing)
        {
            intensity += direction * chancebloomspeed * Time.deltaTime;
            
            if (Mathf.Abs(intensityMax - intensity) <= differenceBetween)
            {
                ChanceTheDirection();
            }
        }
        else
        {
            intensity += direction * chancebloomspeed * Time.deltaTime;
            if (Mathf.Abs(intensityMin - intensity) <= differenceBetween)
            {
                ChanceTheDirection();
            }
        }
        material.EnableKeyword("_EMISSION");
        material.SetVector("_EmissionColor", color * intensity);
    }
    private void ChanceTheDirection()
    {
        isIncreasing = !isIncreasing;
        direction = -direction;
    }
    private void OnApplicationQuit()
    {
        material.SetVector("_EmissionColor", defaultVector);
        Debug.Log("Quitting the game...");
    }
}
