﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Report : MonoBehaviour
{
    private string path;
    private string thereport;
    private StreamWriter _reportFile;
    private bool FileCreated;
    void Start()
    {
        
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene().name == "_SelectionMenu")
        {
            if (!FileCreated)
            {
                CreateText();
                FileCreated = true;
            }
        }
    }

    void CreateText()
    {
        //File Directory
        path = Application.persistentDataPath + "/Relatorios/Report-" + System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + ".txt";
        //Checks if file exists. If it doesn't, creates it.
        if (!File.Exists(path))
        {
            File.WriteAllText(path, "Relatório de " + System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + ": \n\n");
        }
    }

    public void Addingtoreport(string moreinformatedadd, int situation)
    {
        //moreinformatedadd += "pela " + timesanswered[situation] + "ª vez;";
        thereport += moreinformatedadd;
        //timesanswered[situation]++;
        Debug.Log(thereport);
    }
    public void GetFillofthereport()
    {
        string[] information = thereport.Split(';');
        File.AppendAllLines(path, information);
    }
}
