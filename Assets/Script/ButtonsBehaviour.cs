﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonsBehaviour : MonoBehaviour
{
    
    private void Awake()
    {
        
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void GoToFirstGame() // Load Game Scene
    {
        SceneManager.LoadScene("Labirinto1");
    }
    public void GoToNextPhase()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    
    public void GoToThirdPhase() // Load Game Scene
    {
        SceneManager.LoadScene("Labirinto3");
    }
    public void GoToReports() // Load Report Scene
    {
        SceneManager.LoadScene("ReportScene");
    }
    public void GoToSecondPhase()
    {
        SceneManager.LoadScene("Labirinto2");
    }
}
