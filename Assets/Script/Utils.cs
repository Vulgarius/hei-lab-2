﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils : MonoBehaviour
{
    public static Vector3 ScreenToWorld(Camera camera, Vector3 position)
    {
        position.z = camera.nearClipPlane; // The near clipping plane is nearest point of the Camera's view frustum
        // camera.ScreenPointToRay(position); in case of a 2D project
        // if it 
        return camera.ScreenToWorldPoint(position);
        //return camera.ScreenPointToRay(position).direction;
    }
}
