﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Text UItext;
    private float maxSeconds = 60f;
    [SerializeField]private int maxMinutes = 5;
    [SerializeField]private float currentSeconds;
    [SerializeField]private int currentMinutes;
    private int objectesPicked;
    public static GameManager gmInstance;
    [SerializeField] private Image EndPanel;
    [SerializeField] private Text completeText;
    [SerializeField] private Text GetText;
     private int amountPoints;
    [SerializeField] private Text informationText;
    private Animator informationTextAnimator;
    [SerializeField] private Image informationTextPanel;
    private Animator informationTextPanelAnimator;
    //[SerializeField] private Text pointsText;
    private AudioSource audioSource;
    private int points = 0;
    [SerializeField] private AudioClip newGame;
    [SerializeField] private AudioClip death;
    private bool isLevelEnded = false;
    [SerializeField] private Button nextButton;
    public bool isPlaying = false;
    // Start is called before the first frame update
    void Start()
    {
        currentMinutes = maxMinutes;
        currentSeconds = 0;
        objectesPicked = 0;
        // A singletton
        gmInstance = this;
        EndPanel.gameObject.SetActive(false);
        int amountremedy = GameObject.FindGameObjectsWithTag("Remedy").Length; // Get the amount of remedies
        amountPoints = FindObjectOfType<PickUpController>().PointsAmount * amountremedy;
        informationTextAnimator = informationText.gameObject.GetComponent<Animator>();
        informationTextPanelAnimator = informationTextPanel.gameObject.GetComponent<Animator>();
        
        Time.timeScale = 0;
        Cursor.visible = true;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isLevelEnded)
        {
            return;
        }
        // When currentseconds reach to 0 and there are a few minutes left
        if (currentSeconds <= 0 && currentMinutes > 0)
        {
            //currentSeconds = 0f;
            currentSeconds += maxSeconds;
            currentMinutes--;
        }
        else if(currentSeconds <= 0 && currentMinutes <= 0) // WHen there's no time
        {
            audioSource.PlayOneShot(death);
            LevelEnded(false);
        }
        else
        {
            currentSeconds -= Time.deltaTime;
        }
        UItext.text = "Tempo: " + currentMinutes.ToString() + ":" + currentSeconds.ToString("00"); 

    }
    public void Decreaseamountofsecondstime(int amounttime, int amountPoints) // Decrease some time when player pick the objects
    {
        currentSeconds -= amounttime;
        
        objectesPicked+= amountPoints;
        IncreasePoints(amountPoints);
    }
    // The point score is increased;
    public void IncreasePoints(int amountPoints)
    {
        points += amountPoints;
        //pointsText.text = "Pontos: " + points.ToString();
    }
    public void LevelEnded(bool WasReached)
    {
        
        if (!EndPanel.gameObject.activeInHierarchy) // Active the EndPanel
        {
            Time.timeScale = 0;
            EndPanel.gameObject.SetActive(true);
            Cursor.visible = true;
            isLevelEnded = true;
            switch (WasReached)
            {
                case true: // If the player reached
                    completeText.text = "Completou? Sim";
                    nextButton.gameObject.SetActive(true); // can play the next level
                    break;
                case false: // if the player didn't reach
                    completeText.text = "Completou? Não";
                    nextButton.gameObject.SetActive(false); // can play the next level
                    break;
            }
            GetText.text = "Pegou " + objectesPicked + "/" + amountPoints + " pontos!";
        }
    }
    private void StartFadeOut()
    {
        informationTextAnimator.SetBool("Fadeout", true);
        informationTextPanelAnimator.SetBool("Fadeout", true);
        var cliptime = informationTextAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;
        StartCoroutine(LateCall(cliptime));
        
        
    }
    IEnumerator LateCall(float timeTodDsable)
    {
        yield return new WaitForSeconds(timeTodDsable);
        informationText.gameObject.SetActive(false);
        informationTextPanel.gameObject.SetActive(false);
    }
    public void ReadyToPlay() // by the button
    {
        Time.timeScale = 1;
        isPlaying = true; // know the swipes can be read
        audioSource.PlayOneShot(newGame);
        StartFadeOut();
    }
}
