﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhasesInformation : MonoBehaviour
{
    public struct Phase
    {
        private bool wasFinished;
        private int drugsGot;
        private int secondsleft;
    }
    public Phase[] phases;
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
}
