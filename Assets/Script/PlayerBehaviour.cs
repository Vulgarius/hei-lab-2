﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    [SerializeField] private float speed = 5f;
    [SerializeField] private float fallMultiplier = 2.5f;
    private Rigidbody rb;
    private float movX;
    private float movZ;
    //private float targetAngle;
    //[SerializeField] private float angleMin = -45;
    //[SerializeField] private float angleMax = 45;
    //[SerializeField] private float TurnSmoothTime = .1f;
    //private float TurnSmoothVelocity;
    private Vector3 playerDirection;
    //[SerializeField]private bool needToRotate;
    private Vector3 direction;
    public bool canMoveLeft;
    public bool canMoveRight;
    public bool canMoveUp;
    public bool canMoveDown;
    public bool isPlayedStopped;

    // There variables are for swipe control
    [SerializeField] private float minSwipeDistance = 10f;

    [SerializeField]private float maxSwipeTime = 0.7f;
    private float swipeTime; 

    private float swipeStartTime;
    private float swipeEndTime;
    private Vector3 startSwipePosition;
    private Vector3 endSwipePosition;
    private float swipeLength;
    [SerializeField] private float directionThreshhold = 0.9f;
    [SerializeField] private bool detectSwipeOnlyAfterRelease = false;
    private float auxSwipe;

    [SerializeField]private InputManager inputManager;
    public enum Moving
    {
        Stop,isMovingLeft, isMovingRight, isMovingUp, isMovingDown,
    }
    
    public Moving currentMoving;
    public Moving previousMoving;
    private void Awake()
    {
        inputManager = InputManager.instance;
    }
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        previousMoving = Moving.Stop;
    }
    /*private void OnEnable()
    {
        inputManager.OnStartTouch += SwipeStart;
        inputManager.OnEndTouch += SwipeEnd;
    }
    private void OnDisable()
    {
        inputManager.OnStartTouch -= SwipeStart;
        inputManager.OnEndTouch -= SwipeEnd;
    }*/
    private void SwipeStart(Vector2 position, float time)
    {
        startSwipePosition = position;
        swipeStartTime = time;
    }
    private void SwipeEnd(Vector2 position, float time)
    {
        endSwipePosition = position;
        swipeEndTime = time;
        //SwipeTest();
    }
    // Update is called once per frame
    void Update()
    {
        if (!GameManager.gmInstance.isPlaying)
        {
            return;
        }
        SwipeTest();

        //Direction();
        // In relation of the camera
        playerDirection = direction * speed * Time.deltaTime;
        playerDirection = Camera.main.transform.TransformDirection(playerDirection);
        playerDirection.y = 0.0f;


        //Falling down
        //if (rb.velocity.y < 0)
        //{
        //    rb.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.fixedDeltaTime;
        //}
        Move();


    }
    
    private void Direction()
    {
        //SwipeTest();
        
    }
    private void Move()
    {
        // move the player
        rb.velocity += playerDirection;
        isPlayedStopped = false;

        
    }
    private void SwipeTest()
    {
        //if(Vector3.Distance(startSwipePosition, endSwipePosition) >= minSwipeDistance &&
        //    (swipeEndTime - swipeStartTime) <= maxSwipeTime)
        //{
        //    Vector3 swipeDirection = (endSwipePosition - startSwipePosition);
        //    Vector2 swipeDirection2D = new Vector2(swipeDirection.x, swipeDirection.y);
        //    swipeDirection = Camera.main.transform.InverseTransformDirection(swipeDirection);
        //    //if(swipeDirection.sqrMagnitude > minSwipeDistance) // check if squared length of swipedirection is biggger than the minswipedistance
        //    //{
        //        Debug.Log(swipeDirection);
        //        SwipeControl(swipeDirection);
        //    //}

        //}
        if (Input.touchCount > 0) // if has at least one touch 
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began) // When the touch begin
            {
                swipeStartTime = Time.time;
                startSwipePosition = touch.position;

            }
            if (!detectSwipeOnlyAfterRelease && touch.phase == TouchPhase.Moved) // When the touched is moved
            {
                swipeEndTime = Time.time;
                endSwipePosition = touch.position;
                swipeTime = swipeEndTime - swipeStartTime; // see the interval time
                swipeLength = (endSwipePosition - startSwipePosition).sqrMagnitude;
                if (swipeTime < maxSwipeTime && swipeLength > minSwipeDistance)
                {
                    SwipeControl();
                }
            }
            if (touch.phase == TouchPhase.Ended) // When the touch Ends
            {
                swipeEndTime = Time.time;
                endSwipePosition = touch.position;
                swipeTime = swipeEndTime - swipeStartTime; // see the interval time
                swipeLength = (endSwipePosition - startSwipePosition).sqrMagnitude;
                if (swipeTime < maxSwipeTime && swipeLength > minSwipeDistance)
                {
                    SwipeControl();
                }
            }
        }
    }
    private void SwipeControl(/*Vector3 swipedirection*/)
    {
        Debug.Log("Control");
        Vector2 distance = endSwipePosition - startSwipePosition;
        float distanceX = Mathf.Abs(distance.x);
        float distanceY = Mathf.Abs(distance.y);
        if(distanceX > distanceY)
        {
            if(distance.x > 0 && canMoveRight)
            {
                Debug.Log("Direita");
                currentMoving = Moving.isMovingRight;
                if (currentMoving != previousMoving)
                {
                    rb.velocity = Vector3.zero;
                    previousMoving = currentMoving;
                    direction = Vector3.right;
                }
            }
            else if(distance.x < 0 && canMoveLeft)
            {
                Debug.Log("Esquerda");
                currentMoving = Moving.isMovingLeft;
                if (currentMoving != previousMoving)
                {
                    rb.velocity = Vector3.zero;
                    previousMoving = currentMoving;
                    direction = Vector3.left;
                }
            }
        }
        else if(distanceY > distanceX)
        {
            if(distance.y > 0 && canMoveUp)
            {
                currentMoving = Moving.isMovingUp;
                if (currentMoving != previousMoving)
                {
                    rb.velocity = Vector3.zero;
                    previousMoving = currentMoving;
                    direction = Vector3.up;
                }
            }
            else if (distance.y < 0 && canMoveDown)
            {
                currentMoving = Moving.isMovingDown;
                if (currentMoving != previousMoving)
                {
                    rb.velocity = Vector3.zero;
                    previousMoving = currentMoving;
                    direction = Vector3.down;
                }
            }
        }
        //if(Vector2.Dot(Vector2.up,swipedirection) > directionThreshhold)
        //{
        //    currentMoving = Moving.isMovingUp;
        //    if (currentMoving != previousMoving)
        //    {
        //        rb.velocity = Vector3.zero;
        //        previousMoving = currentMoving;
        //        direction = Vector3.up;
        //    }
        //}
        //else if (Vector2.Dot(Vector2.down, swipedirection) > directionThreshhold)
        //{
        //    currentMoving = Moving.isMovingDown;
        //    if (currentMoving != previousMoving)
        //    {
        //        rb.velocity = Vector3.zero;
        //        previousMoving = currentMoving;
        //        direction = Vector3.down;
        //    }
        //}
        //else if (Vector2.Dot(Vector2.right, swipedirection) > directionThreshhold)
        //{
        //    Debug.Log(swipedirection.z + "- swipeddireztion.z");
        //    Debug.Log("Direita");
        //    currentMoving = Moving.isMovingRight;
        //    if (currentMoving != previousMoving)
        //    {
        //        rb.velocity = Vector3.zero;
        //        previousMoving = currentMoving;
        //        direction = Vector3.right;
        //    }
        //}
        //else if (Vector2.Dot(Vector2.left, swipedirection) > directionThreshhold)
        //{
        //    Debug.Log(swipedirection.z + "- swipeddireztion.z");
        //    Debug.Log("Esquerda");
        //    currentMoving = Moving.isMovingLeft;
        //    if (currentMoving != previousMoving)
        //    {
        //        rb.velocity = Vector3.zero;
        //        previousMoving = currentMoving;
        //        direction = Vector3.left;
        //    }
        //}
        //if(Mathf.Abs(swipedirection.x) > Mathf.Abs(swipedirection.y))
        //{
        //    if (swipedirection.x < 0 /*&& canMoveUp*/) // Go Up
        //    {
        //        currentMoving = Moving.isMovingUp;
        //        if (currentMoving != previousMoving)
        //        {
        //            rb.velocity = Vector3.zero;
        //            previousMoving = currentMoving;
        //            direction = Vector3.up;
        //        }

        //    }
        //    else if (swipedirection.x > 0 /*&& canMoveDown*/) // Go Down
        //    {
        //        currentMoving = Moving.isMovingDown;
        //        if (currentMoving != previousMoving)
        //        {
        //            rb.velocity = Vector3.zero;
        //            previousMoving = currentMoving;
        //            direction = Vector3.down;
        //        }

        //    }
        //}
        //else if (Mathf.Abs(swipedirection.x) < Mathf.Abs(swipedirection.y)) // Go Left
        //{

        //    if(/*auxSwipe < 0*/ swipedirection.y > 0 /*&& swipedirection.x < 0) && canMoveLeft*/)
        //    {
        //        Debug.Log(swipedirection.z + "- swipeddireztion.z");
        //        Debug.Log("Esquerda");
        //        currentMoving = Moving.isMovingLeft;
        //        if (currentMoving != previousMoving)
        //        {
        //            rb.velocity = Vector3.zero;
        //            previousMoving = currentMoving;
        //            direction = Vector3.left;
        //        }
        //    }
        //    else if(/*auxSwipe > 0*/ swipedirection.y < 0 /*&& endSwipePosition.z <0 *//*&& canMoveRight*/) // Go right
        //    {
        //        Debug.Log(swipedirection.z + "- swipeddireztion.z");
        //        Debug.Log("Direita");
        //        currentMoving = Moving.isMovingRight;
        //        if (currentMoving != previousMoving)
        //        {
        //            rb.velocity = Vector3.zero;
        //            previousMoving = currentMoving;
        //            direction = Vector3.right;
        //        }
        //    }
        //}

        /*else *///if (Vector2.Dot(Camera.main.transform.InverseTransformDirection(Vector3.left), swipedirection) > directionThreshhold)
                 //if (Vector2.Dot(Vector3.up, swipedirection) > directionThreshhold)
                 //{

        //    Debug.Log("Esquerda");
        //    currentMoving = Moving.isMovingLeft;
        //    if (currentMoving != previousMoving)
        //    {
        //        rb.velocity = Vector3.zero;
        //        previousMoving = currentMoving;
        //        direction = Vector3.left;
        //    }
        //}
        /*else */
        //if (Vector2.Dot(-(Vector3.down), swipedirection) > directionThreshhold)
        //{

        //    Debug.Log("Direita");
        //    currentMoving = Moving.isMovingRight;
        //    if (currentMoving != previousMoving)
        //    {
        //        rb.velocity = Vector3.zero;
        //        previousMoving = currentMoving;
        //        direction = Vector3.right;
        //    }

        //}


    }
    public void CanMoveHorizontal()
    {
        canMoveLeft = true;
        canMoveRight = true;
    }
    public void CanMoveVertical()
    {
        canMoveUp = true;
        canMoveDown = true;
    }
    public void CannotMoveHorizontal()
    {
        canMoveLeft = false;
        canMoveRight = false;
    }
    public void CannotMoveVertical()
    {
        canMoveUp = false;
        canMoveDown = false;
    }
    public void CannotMoveUp()
    {
        canMoveUp = false;
        canMoveDown = true;
    }
    public void CannotMoveDown()
    {
        canMoveUp = true;
        canMoveDown = false;
    }
    public void CannotMoveLeft()
    {
        canMoveLeft = false;
        canMoveRight = true;
    }
    public void CannotMoveRight()
    {
        canMoveRight = false;
        canMoveLeft = true;
    }
    public void StopVelocity()
    {
        if (!isPlayedStopped)
        {
            direction = Vector3.zero;
            rb.velocity = Vector3.zero;
            isPlayedStopped = true;
        }
        
    }

}
