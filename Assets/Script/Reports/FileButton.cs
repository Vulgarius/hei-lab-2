﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FileButton : MonoBehaviour
{
    [SerializeField] private int dataBase;
    // Start is called before the first frame update
    void Start()
    {
        //Show the file with the same index as the one in this button's name

        GameObject.FindGameObjectWithTag("ReportScreen").GetComponent<FileReportsScreen>().Showfile(dataBase);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetIndex(int index)
    {
        dataBase = index;
    }
}
