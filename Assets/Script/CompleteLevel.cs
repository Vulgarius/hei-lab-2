﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompleteLevel : MonoBehaviour
{

    private int finalAmountPoints = 300;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player")) // The game is over when the player reach the final end
        {
            GameManager.gmInstance.IncreasePoints(finalAmountPoints);
            GameManager.gmInstance.LevelEnded(true);
        }
    }
}
