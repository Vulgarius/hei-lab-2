﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PickUpController : MonoBehaviour
{
    //[SerializeField] private float PickUpRange = 20f;
    [SerializeField] private int decreaseamountTime = 5;
    [SerializeField] private int amountOfPoints = 10;
    public int PointsAmount
    {
        get { return amountOfPoints; }
    }
    private PostProcessVolume processVolume;
    private float bloomIntensityValue;
    [SerializeField]private float bloomIntensityMax = 4f;
    
    Bloom bloomLayer;
    // Start is called before the first frame update
    void Start()
    {
        processVolume = GetComponent<PostProcessVolume>();
        processVolume.profile.TryGetSettings(out bloomLayer);
        bloomIntensityValue = bloomIntensityMax;
        if(bloomLayer == null)
        {
            Debug.LogWarning("Não há bloomLayer!!");
        }
        if (!bloomLayer.enabled)
        {
            bloomLayer.enabled.value = true;
        }
        bloomLayer.intensity.value += bloomIntensityValue;
    }

    // Update is called once per frame
    void Update()
    {
        
        

    }
    private void PickUp() // Player is gonna PickUp the object
    {
        this.gameObject.SetActive(false);
        GameManager.gmInstance.Decreaseamountofsecondstime(decreaseamountTime, amountOfPoints);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            PickUp();
        }
    }
    
}
